<?php
/*
 * // product:   fetchTweets 
 * @author     		Jorge Pereira
 * @copyright  		2012-2015 enterprisal LLC
 * @website	   	http://enterprisal.com/
 * @productpage	   	http://enterprisal.com/fetchtweets
 * @moreinfo   		info@enterprisal.com
 *
 */
/*
Revision History: 
v4e2 - 12/31/2015
      - Code released to the community as Open Source. https://bitbucket.org/jorgep/fetchtweets

v4e1 - 1/3/2015
	- Added javascript to populate  start/end dates based on last 7,10,30,60,120  radio button.
v4e - 1/3/2015
	- Added debug log function and section. 
	- fixed the date range returning wrong results issue ( date formats were incorrect. )
	
v4d - 1/2/2014 -- 
	- multiple changes and fixes
	- date rante and date limiit now decoupled from number of returned tweets

v4c - 12/15/2014 -- 
	- removed switchContent routine
	- Move into enterprisal llc  / release as GPL GNUv3
v4a - 12/15/2013 --  
	- Changed the API call  to OAth based authentication 
	- Added  Date Ranges.
v2a -   Changed the API call to: 'http://api.twitter.com/1/statuses/user_timeline/'

*/

session_start();
require_once('twitteroauth-master/twitteroauth/twitteroauth.php');

function debugLog1($txt)
{
		$filename = "0debuglog.txt";
		$fp = fopen($filename,'a');		
		fwrite($fp,'['.date('Ymd His')."] : ".$txt."\n");
		fclose($fp);	
}


?>
<html>
<head>
<meta name="description" content="fetchTweets is an easy to use webapp to retrieve tweets in a simple html table format for archive and backup.">
<meta name="author" content="enterprisal.com">
<meta name="keywords" content="twitter, save tweets, tweet, tweets, backup, fetch, save, fetchtweets, export, download">
<title>fetchTweets</title>


 <style type="text/css">
	#d_clip_button {
			text-align:center;
			border:1px solid black;
			background-color:#ccc;
			margin:10px; padding:10px;
			width:250px;
	}
	#d_clip_button.hover { background-color:#eee; }
	#d_clip_button.active { background-color:#aaa; }
	 <!--
	 TD{font-family: Arial; font-size: 9pt;}
	 --->

 </style>
 <script type="text/javascript" src="ZeroClipboard.js"></script>
 <script type="text/javascript"> 
function foot(w)
{

if(window.XMLHttpRequest)
{
var x= new XMLHttpRequest();
}
else
{
var x= new ActiveXObject('Microsoft.XMLHTTP');
}

x.onreadystatechange=function()
{if(x.readyState==4 && x.status==200)
{
document.getElementById("arce").innerHTML= x.responseText;

var st=document.getElementById("str").innerHTML;
var en=document.getElementById("end").innerHTML;
document.getElementById("startdate").value=st;
document.getElementById("enddate").value=en;
}
}

x.open('POST', 'daterange.php?id='+w, false);
x.send();
}
</script>

</head>

<body>

<?php
// $prog_name = basename( $_SERVER['PHP_SELF'], ".php");
// $prog_version = substr( $prog_name, strpos( $prog_name, '_')+1, strlen ($prog_name)  );
// $prog_name = substr( $prog_name, 0, strpos( $prog_name, '_') );

$prog_name    = "fetchTweets";
$prog_version = "v4e2";
$debuglog=false;
$txt="";

if(isset($_POST['handle1']))	{ $handle1 = $_POST['handle1']; $debuglog=true; } else { $handle1 =""; };
if(isset($_POST['number1']))	{ $number1 = $_POST['number1'];  } else { $number1 ="";};
if(isset($_POST['start_date']))	{ $start_date = str_replace("-","/",$_POST['start_date']); } else { $start_date ="";};
if(isset($_POST['end_date']))	{ $end_date = str_replace("-","/",$_POST['end_date']);  } else { $end_date =""; };
if(isset($_POST['date_limit']))	{ $datelimit = $_POST['date_limit'];  } else { $datelimit ="";};

 if ($debuglog) { 
	
	$txt= $handle1;
	$txt .= ",".$number1;
	$txt .= ",".$start_date;
	$txt .= ",".$end_date;
	$txt .= ",".$datelimit;	
	debugLog1($txt) ; 
  };



?>
<h1><?php echo "" . $prog_name  ?>&nbsp;&nbsp;<span style='font-size : small;'>(<?php echo " " . $prog_version ?>)</span></h1>
<h2></h2>


<!--
 For copy to clipboard I used:
       http://code.google.com/p/zeroclipboard/
-->

<table width="950px" border="0"><tr><td>
<!------------WHEN YOU GIVE ACTION EMPTY IT WILL WORK ONLY ON THIS PAGE LIKE PHP_SELF------------>
<form method="post" action="" >
<table width="550px" border="0">
  <tr>
    <td width="175px">Enter Twitter Handle:</td>
    <td width="375px"><input type="text" size="30" maxlength="30" name="handle1" value=<?php echo $handle1 ?>></td>
  </tr>
   <tr>
    <td colspan="2"><strong>AND</strong></td>
    </tr>
  <tr>
  <tr>
    <td>Number of tweets to return:</td>
    <td><input type="text" size="5" maxlength="5" name="number1" value=<?php echo $number1 ?>><small>(max: 200)</smal</td>
  </tr>
  <tr>
    <td colspan="2"><strong>OR</strong></td>
    </tr>
  <tr>
    <td>Start Date:</td>
    <td><input type="text" id="enddate" name="start_date" value=<?php echo $start_date ?>> <small>(format: dd/mm/yyyy)</small></td>	
  </tr>
  <tr>
    <td> End Date:</td>
    <td><input type="text" id="startdate" name="end_date" value=<?php echo $end_date ?>></td>
  </tr>
  <tr>
    <td colspan="2"><strong>Quick date chooser:</strong></td>
   </tr>
   <tr>
    <td colspan="2">
      <input name="date_limit" type="radio"  value="7" onClick="foot(7)"> Last 7 days |  
      <input name="date_limit" type="radio" value="10" onClick="foot(10)"> Last 10 days |   
      <input name="date_limit" type="radio"  value="30" onClick="foot(30)"> Last 30 days |   
      <input name="date_limit" type="radio" value="60" onClick="foot(60)"> Last 60 days |  
      <input name="date_limit" type="radio" value="120" onClick="foot(120)"> Last 120 days   
    </td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td align="left"><input type="submit" name="submit"></td>
  </tr>
</table>
</form>


</td>
<td>
<div>
<h3> About This Tool</h3>
<small>This is a PHP script to retrieve / "fetch" tweets from an account and returns the results in a simple HMTL table with no formatting that you can copy and paste into the clipboard. <br>
Originally created to archive / backup tweets into a blog by 1) entering a handle,  2) enter either a date range or the number of tweets to retrieve and 3) copy and paste results into blog.<br><br>
Keywords: twitter, tweets, tweet, save tweets, twitter archives, tweet archives,  twitter backup,  tweet backups, fetchtweets

<br>
<h3>Usage: </h3>
<ul>
<li>Just enter the twitter handle (or hash), the date range (or number of tweets to retrieve)   and voila!</li>
</ul>
<br>
</small>
</div>
</td>
</tr>
</table>
<?php require_once('twitteroauth-master/tweets_table.php'); ?>

<?php
$http		= $_SERVER['HTTPS'] = 'on' ? 'https://':'http://';
$http		= 'http://';
$text       = "Found a simple webapp for backing up tweets ";
$url		= urlencode("http://goo.gl/PHcC0J");
$screen_name	= 'enterprisal';  // use your Twitter screen name
$hashtags 	= "fetchtweets,webapp";  // comma separated list of hashtags (without #) to automatically be inserted into the tweet
?>
<div>
<br><br>
<small>This web-based tool / service  is provided free of charge.  Please read our <a target="_blank"  href="termsofservice.php" >Terms of Service</a>  before using.
<br><br>
Please send <a target="_blank" href="https://twitter.com/intent/tweet?text=@enterprisal+suggestion+on&hashtags=fetchteets+">suggestions</a> to Twitter handle: <a target="_blank" href="https://twitter.com/enterprisal">@enterprisal</a> using:#fetchTweets<br>
Developed by Jorge Pereira. Source code information, customization and licensing is available at <a target="_blank" href="http://enterprisal.com/fetchtweets"> enterprisal llc</a><br>
<br>As of 12/31/2015 We have release the source code to the community as Open Source. Review and contribute at:  <a target="_blank" href="https://bitbucket.org/jorgep/fetchtweets">https://bitbucket.org/jorgep/fetchtweets </a><br>
<br>
<a target="_blank"  href="termsofservice.php" >Terms of Service</a> | Help us by spreading the world! <a target="_blank"  href="http://twitter.com/intent/tweet?text=<?php echo $text; ?>&url=<?php echo $url; ?>&hashtags=<?php echo $hashtags; ?>">Tweet This</a>

<small><br>
</p>
</div>
<div id="arce" style="visibility:hidden;border:1px solid #ccc;"></div>

<script type="text/javascript">


// Initialize Copy to clipboard
      var clip = new ZeroClipboard.Client();
      clip.setText( document.getElementById('clip_text').innerHTML );
      clip.glue( 'd_clip_button' );
</script>



</BODY>
</html>