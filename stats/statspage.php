<?php 
session_start(); 
session_set_cookie_params(3600);
?>
<html>
<head>
<style>
body {
    font: normal 11px Verdana, Arial, sans-serif;
}
p.serif {
    font-family: "Times New Roman", Times, serif;
}

p.sansserif {
    font-family: Arial, Helvetica, sans-serif;
}
table.table1 {
  width: 80%;
  max-width: 80%;
  vertical-align: top;
border-collapse: collapse;
font-family: Verdana, Geneva, sans-serif;
}

table.table1 tr, td {
border: solid 1px black;
border-collapse: collapse	
font-size: 9px;
}
table.table1 th{
border: solid 3px black;
border-collapse: collapse
font-size: 9px;
}

table.blueTable {
  font-family: Arial, Helvetica, sans-serif;
  border: 1px solid #1C6EA4;
  background-color: #EEEEEE;
  width: 80%;
  text-align: left;
  border-collapse: collapse;
}
table.blueTable td, table.blueTable th {
  border: 1px solid #AAAAAA;
  padding: 3px 2px;
}
table.blueTable tbody td {
  font-size: 11px;
}
table.blueTable tr:nth-child(even) {
  background: #D0E4F5;
}
table.blueTable thead {
  background: #1C6EA4;
  border-bottom: 2px solid #444444;
}
table.blueTable thead th {
  font-size: 11px;
  font-weight: bold;
  color: #FFFFFF;
  border-left: 2px solid #D0E4F5;
}
table.blueTable thead th:first-child {
  border-left: none;
}

table.blueTable tfoot td {
  font-size: 14px;
}
table.blueTable tfoot .links {
  text-align: right;
}
table.blueTable tfoot .links a{
  display: inline-block;
  background: #1C6EA4;
  color: #FFFFFF;
  padding: 2px 8px;
  border-radius: 5px;
}

</style>
</head>
<body>

<?php


$ok=False;
$passw="appstats";

function goUrl($path) 
{
?>
<script type="text/javascript">
	window.location.href = '<?php echo $path; ?>';
 </script>
<?php
}

Echo "<h2>Stats Dashboard</h2>";


if (!isset($_SESSION['DSdashboard']) ) {
 if(!isset($_POST['pass']))
    {?>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
            Password <input type="password" name="pass"></input><br/>
            <input type="submit" name="submit" value="Go"></input>
            </form>
    <?} else {
        if ($_POST['pass']==$passw)
        { $ok = true; $_SESSION["DSdashboard"] ="set"; } else
           {goUrl( $_SERVER['PHP_SELF'] );exit;}
    } 
}else { $ok=true;}

If ($ok) {

echo '<a href="logout.php">click here to log out</a><br>';

//  ***** LOGIC BEGINS HERE!    

$folder = './data';
$folder1 ='/home3/jorgep98/public_html/dayscheduler.com/app/prod2/customerfiles/';

echo "<br><hr><br>";
echo "<h2> Daily traffic Statistics:</h2><br>";

echo ListallFiles("./data/");


}


Function getLastAccess($file,$where)
{
$txt="--";

$file= '/home3/jorgep98/public_html/dayscheduler.com/app/prod2/customerfiles/'.$where.'/'.$file;


if (file_exists($file)) {
  $handle=fopen($file, 'r');
  $txt = fgets($handle); // read until first newline
  fclose($handle);

}
return $txt;
}

function CheckInstance( $what, $where ) 
{ // function checks instance and returns values
//  type: 
//  LastUsed: 
//  LastAdmin:

$txt="--";
$file= '/home3/jorgep98/public_html/dayscheduler.com/app/prod2/customerfiles/'.$where.'/customervariables.php';

if (file_exists($file)) { 
   $handle = fopen($file, 'r');
   $valid = false; // init as false
   while (($buffer = fgets($handle)) !== false) {
    if (strpos($buffer, $what) !== false) {
         $valid = TRUE;
         $txt ="Found:".$what;
         $txt=substr($buffer, strpos($buffer, $what)+10 , 4);
         break; // Once you find the string, you should break out the loop.
        }      
   }
   fclose($handle);
}

Return $txt;
}
 
function GetUnconfirmedNumber( $file ) 
{ // function returns arrays of values 
  $input = fopen($file, 'r');  //open for reading
 //  echo $input;
  $count1=0;
  $count0=0;

  
  while( false !== ( $data = fgetcsv($input) ) ){  //read each line as an array
      // Fields: 0=name 1=instancename 2=email 3= 4=dateregistered  5=date-confirmed
        $count0++;
	//  echo $data[0]." / ".$data[1]."<br>";

	 //  if ($data[5] != '') {
          if (empty($data[5]) ) {
		$count1++; 
	   }
  }
return array ($count0,$count1);
}



function getURL()
//getURL function to get URL 
 { 
  /* First check if page is http or https */
    // $whichprotocol = $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';
	 $whichprotocol = "http";
  /* Combine different pieces of $_SERVER variable to return current URL */
     return $whichprotocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
 }

function array_unique_deep($array, $key)
{
/**
 * Extract unique values of the specified key in a two dimensional array
 *
 * @param array $array
 * @param mixed $key
 * @return array
 */

  $values = array();
  foreach ($array as $k1 => $row)
  {
    foreach ($row as $k2 => $v)
    {
      if ($k2 == $key)
      {
        $values[ $k1 ] = $v;
        continue;
      }
    }
  }
$values = array_values(array_filter(array_unique($values)));
  return $values;
}


 Function ListallFiles($globaldir)
 {
	 
//	 $files = glob("/path/to/directory/*.{jpg,gif,png}", GLOB_BRACE);
	 $files = glob($globaldir."/databackup*.{zip}", GLOB_BRACE);
	 $files = glob($globaldir."/backups/*.{bkp}", GLOB_BRACE);
	 $files = glob($globaldir."cy*.{csv}", GLOB_BRACE);
	 

	  $html= "";
	  // $html.= $globaldir."<br><br>";
	  $html.= "<b>Log Archives:</b> "."<br>";
	  $displayfilename="";
	  if (empty($files)) {
		  $html .= "No files found";
	  }else{
	  arsort($files);
	  $html = "<div style='width: 40%; background-color: white;'>";
	  $html .='<table class="blueTable"><tr><td>Date</td><td>Page Views</td><td>unique IPs</td><td>Referrers</td><td> - </td></tr>';
		 foreach($files as $key => $value)
			{
				$displayfilename = str_replace($globaldir,"",$value);
				// $displayfilename = $value;
		
                // $url0=str_replace($_SERVER['REQUEST_URI'],"/data/".substr($globaldir, strrpos($globaldir, '/') + 1) . "/".$displayfilename,getURL());;
                $url0 = "#";
                $file1=$globaldir.$displayfilename;
                // $csv = array_map('str_getcsv', file($file1));
                $rows = array_map('str_getcsv', file($file1));
                $header = array_shift($rows);
                $csv = array();
                foreach ($rows as $row) {
                  $csv[] = array_combine($header, $row);
                }
                
                If (0){
                 echo "<pre>";
                 print_r ($csv);
                // print_r ("<pre>".unique($csv,"IP"."</Pre>"));
                // print_r (array_unique_deep($csv, "IP"));
                 echo "</pre><BR>";
                }
			    // $html.= "<a href='".$url0."'>".$displayfilename." </a>"."  (";
			    if (!empty(array_unique_deep($csv, "DATE")[0])) {
			    $html .="<tr><td>".array_unique_deep($csv, "DATE")[0]."</td><td>".count($csv)."</td><td>".count(array_unique_deep($csv, "IP"))."</td><td>".count(array_unique_deep($csv, "REFERRER"))."</td><td> - </td></tr>";
			    }
			    
                            // echo $value;
			}
			$html .="</table></div><br>Work in Progress";
	  }

Return ($html)	 ;
 }



?>

</body>
</html>
