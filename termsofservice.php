<?php
/*
 * // product:   fetchTweets 
 * @author     		Jorge Pereira
 * @copyright  		2012-2015 enterprisal LLC
 * @website	   		http://enterprisal.com/
 * @productpage	   	http://enterprisal.com/fetchtweets
 * @moreinfo   		info@enterprisal.com
 *
 */

?>
<html>
<head>
<meta name="author" content="enterprisal.com">

<title>fetchTweets Terms of Service & Privacy Policy</title>
<p><strong>Terms of Use and Privacy Policy</strong></p>
<p>fetchTweets (the site, the website) is an easy to use webapp to retrieve tweets in a simple html table format for archive and backup.</p>
<p>By using this site, you AGREE to these Terms of Service and Privacy Policy&nbsp;</p>
<p>We use read only access to your Twitter account (official <a href="http://apiwiki.twitter.com/Authentication">Twitter API info </a> ).</p>
<p>We do not capture your Twitter account access. </p>
<p>We do not use your Twitter name in promotional materials and marketing.</p>
<p>As with most other websites, we collect and use the data contained in log files. The information in the log files include your IP (internet protocol) address, your ISP (internet service provider, such as AOL or Shaw Cable), the browser you used to visit our site (such as Internet Explorer or Firefox), the time you visited our site and which pages you visited throughout our site.</p>
<p>We do use cookies to store information, such as your personal preferences when you visit our site.</p>
<p>You can chose to disable or selectively turn off our cookies or third-party cookies in your browser settings, or by managing preferences in programs such as Norton Internet Security. However, this can affect how you are able to interact with our site as well as other websites. This could include the inability to login to services or programs, such as logging into forums or accounts.</p>
<p>We do not use third-party advertising companies to serve ads when you visit our website.</p>
<p>All content is subject to change and is provided to you "as is" without any warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose, or noninfringement. </p>
<p>Your use of the site is at your own risk. Neither the website, nor any of its subsidiaries, affiliates, officers or directors, nor any of its agents or any other party involved in creating, producing, or delivering the site or its content, are liable for any direct, indirect, punitive, incidental, special, consequential or other damages arising out of or in any way connected with the use of this site or content whether based on contract, tort, strict liability or otherwise, even if advised of the possibility of any such damages. Please note that some jurisdictions may not allow the exclusion of certain damages, so some of the above exclusions may not apply to you.</p>

<p>This service is provided free of charge<p>
<br>
<p>Source code information, customization and licensing is available at <a href="http://enterprisal.com/fetchtweets">enterprisal llc.</a></p> 


<p>If you have any questions about this document, please  use the <a href="http://enterprisal.com/contactus">contact us form </a> on our site.</p>


</BODY>
</html>