This is a PHP Script to retrieve / "fetch" tweets from an account and returns the results in a simple HMTL table with no formatting that you can copy and paste into the clipboard. 

Originally created to backup tweets into a blog by entering either a date range or the number of tweets to retrieve and paste them.

USAGE: 

Just enter the twitter handle (or hash), the date range (or number of tweets to retrieve) and voila!

SCRIPT REQUIREMENTS:

- PHP v5.3 or above
- no database required
- need to have App Authorization Access Codes for Twitter: 
	- Consumer Key
	- Consumer Secret
	- Access Token
	- Access Secret Token

INSTALLATION: 
- create a directory on your web server
- move all files contained in zip code to that directory
- Modify tweets_table.php  file Section: Access Codes accordingly

CONTACT: 
Questions, information, thoughts or comments:  
	info@enterprisal.com 
	http://enterprisal.com/fetchtweets


